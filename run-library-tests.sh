#!/bin/bash

# MIT License
#
# Copyright (c) 2020-2021 Jair López <jair_lopez4321@hotmail.com> and contributors
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


PHP_SOURCE_CODE="/usr/src/php"

set -e

apply_patches() {
   if [ -f "$PHP_SOURCE_CODE/ext/session/tests/session_regenerate_id_cookie.phpt" ]; then
      mkdir -p tests/with{,out}-args/sapi/cgi/tests
      cp "$PHP_SOURCE_CODE/sapi/cgi/tests/include.inc" tests/with-args/sapi/cgi/tests
      cp "$PHP_SOURCE_CODE/sapi/cgi/tests/include.inc" tests/without-args/sapi/cgi/tests
   fi

   while [[ $# -gt 0 ]]; do
      local php_version=$1
      local dir_name=

      if [[ -n "$php_version" ]]; then
         dir_name="patches/below-"$(tr -d "." <<< "$php_version")

         if [[ ! -d "$dir_name" ]] || php -r 'exit(version_compare(phpversion(), "'$php_version'") >= 0 ? 0 : 1);'; then
            dir_name=
         fi
      elif [[ -d patches ]]; then
         dir_name=patches
      fi

      if [[ -n "$dir_name" ]]; then
         find "$dir_name" -mindepth 1 -maxdepth 3 \( \( -type d -not \( -name "with-args" -or -name "without-args" -or -name "ours" \) -prune \) -or \( -type f -name "*.patch" -print0 \) \) | while IFS= read -r -d '' file; do
            echo "Applying patch '$file'..."
            patch -fp0 < "$file" || true
            echo ""
         done
      fi

      shift
   done

   if php -r 'exit(version_compare(phpversion(), "7.4") < 0 ? 0 : 1);'; then
      export TEST_PHP_EXECUTABLE=$(which php)

      if [ -d patches/run-tests ] && php -r 'exit(version_compare(phpversion(), "7.1") < 0 ? 0 : 1);'; then
          echo "Processing patches for 'run-tests.php' ..."
          cp "$PHP_SOURCE_CODE/run-tests.php" .

          patch -fp0 < patches/run-tests/run-tests-70.php.patch \
          || patch -fp0 < patches/run-tests/run-tests-56.php.patch \
          || patch -fp0 < patches/run-tests/run-tests-54.php.patch \
          || true

          mv run-tests.php "$PHP_SOURCE_CODE/run-tests.php"
      fi
   fi
}

run_tests() {
   local result=0

   while [[ $# -gt 0 && $result -eq 0 ]]; do
      local dir_name=$1

      if [[ -n "$dir_name" ]]; then
         php "$PHP_SOURCE_CODE/run-tests.php" -q "tests/$dir_name" || result=$?

         if [[ "$result" -eq 1 ]]; then
            # Display expected and actual output from failed tests
            echo "Showing log files for 'tests/$dir_name' ..."

            find "tests/$dir_name" \
                -iname "*.log" \
                -print -exec cat '{}' ';' -exec echo "" ';'
         fi
      else
         result=1
      fi

      shift
   done

   return $result
}

rm -rf tests/with{,out}-args
mkdir -p tests/with{,out}-args

last_dir=
while IFS= read -r; do
   # Get path
   path=${REPLY%/*}
   level=$(sed 's#[^/]##g' <<< "$path")
   require_line='require __DIR__ . "/../../../'

   while [ -n "$level" ]; do
      require_line="$require_line../"
      level=${level%/*}
   done

   require_line="${require_line}lib/session_start_compat.php\";"
   mkdir -p tests/with{,out}"-args/$path"

   if [[ "$last_dir" != "$PHP_SOURCE_CODE/$path" && $(find "$PHP_SOURCE_CODE/$path" -iname "*.inc" -print -quit | wc -l) -gt 0 ]]; then
      last_dir="$PHP_SOURCE_CODE/$path";

      cp -u "$PHP_SOURCE_CODE/$path/"*.inc "tests/with-args/$path"
      cp -u "$PHP_SOURCE_CODE/$path/"*.inc "tests/without-args/$path"
   fi

   echo "Copying and transforming $REPLY..."
   # 1- Require library in tests
   # 2- Replace session_start() with session_start_compat()
   # 3- Make some test less strict because now stack traces will include an extra
   #    level of information and
   #    (a) Line numbers will not match
   #    (b) Some warnings/notices will be reported as if they were caused by
   #        session_start_compat, instead of the invoker
   sed '/^--EXPECTF--$/,${s/on line [0-9]*/on line %d/g; s/%s[^ ]*/%s/g; s/\(%d\)\+/%d/g}' "$PHP_SOURCE_CODE/$REPLY" \
   | sed '/^--FILE--$/,/^--/{/^echo "\*\*\*/n; s/session_start()/session_start_compat()/;s/session_regenerate_id(/session_regenerate_id_compat(/}' \
   | sed '/^--FILE--$/{n;a\
'"$require_line"'
}' > "tests/without-args/$REPLY"

   sed '/^--EXPECTF--$/,${s/on line [0-9]*/on line %d/g; s/%s[^ ]*/%s/g; s/\(%d\)\+/%d/g}' "$PHP_SOURCE_CODE/$REPLY" \
   | sed '/^--FILE--$/,/^--/{/^echo "\*\*\*/n; s/session_start()/session_start_compat(array("cookie_samesite" => "Lax"))/;s/session_regenerate_id(/session_regenerate_id_compat(/}' \
   | sed '/^--FILE--$/{n;a\
'"$require_line"'
}' > "tests/with-args/$REPLY"

   #cp "$PHP_SOURCE_CODE/$REPLY" "tests/with-args/$path"
   #cp "$PHP_SOURCE_CODE/$REPLY" "tests/without-args/$path"
done < <(cd "$PHP_SOURCE_CODE"; grep -wrl --exclude='*' --include='*.phpt'  'session_start()')

apply_patches "" 8.0 7.3 7.2 7.0

run_tests without-args with-args ours

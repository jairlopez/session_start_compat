# Session_start_compat

This library is intended to make it possible to use the PHP 8.0.3 `session_start` and
`session_regenerate_id` API in projects that require PHP 5.6 and above.


## Requirements

It requires `PHP >= 5.6`


## Installation

To install, simply require the `session_start_compat.php` file under `lib`.


## Usage

After installation, two functions named `session_start_compat` and
`session_regenerate_id_compat` are available, which are drop-in replacements for the
built-in [`session_start_compat`](https://www.php.net/manual/en/function.session-start.php) and
[`session_regenerate_id`](https://www.php.net/manual/en/function.session-regenerate-id.php)
respectively.

Let's say a project that requires PHP 5.6 uses the built-in `session_start` in this
way:

    session_start();

The built-in `session_start` doesn't allow developers to specify the `SameSite=Lax`
cookie attribute; for doing that, they can use `session_start_compat`
instead, and take advantage of the function signature available as of PHP 7.3:

    session_start_compat(array(
        'cookie_samesite' => 'Lax'
    ));


## Testing

This library uses PHP's mechanisms for testing purposes.  Download PHP's source
code and unpack it under `/usr/src/php`, then run `./run-library-tests.sh`


## Contribution

Your feedback and contributions are really welcome! If you find any way of
improving it please let me know. There are namely two ways of contributing:

- Sending *Merge requests* which improve the functionality of this library.
  Additional test units are also welcome
- Raising descriptive *Issues*

I have spent a significant amount of time developing and testing it so that it
may also be used in other projects as well. If you find it useful, please
consider [making a donation](https://www.paypal.me/jair4321), I really
appreciate it.

I'm glad you can save time and effort by leveraging this library.


## Security Vulnerabilities

If you have found a security issue, please contact me directly at
[jair_lopez4321@hotmail.com](mailto:jair_lopez4321@hotmail.com).


## License

See `LICENSE.txt` for information about licensing.


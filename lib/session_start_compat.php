<?php
/**
	session_start_compat: A Compatibility library with PHP 8.0.3's
	                      session_start API for projects requiring
			      PHP >= 5.3.

	MIT License

	Copyright (c) 2020-2021 Jair López <jair_lopez4321@hotmail.com> and contributors

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all
	copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
	SOFTWARE.
 */
namespace { // global scope
	if (!class_exists('\Error')) {
		class Error extends \Exception {
		}
	}

	if (!class_exists('\TypeError')) {
		class TypeError extends \Error {
		}
	}

	if (!class_exists('\ArgumentCountError')) {
		class ArgumentCountError extends \TypeError {
		}
	}


	if (!class_exists('\ValueError')) {
		class ValueError extends \Error {
		}
	}

	if (!function_exists('session_start_compat')) {
		/**
		* Drop-in replacement for standard session_start() allowing
		* older versions of PHP to start sessions using the PHP 8.0.3's
		* session_start API
		*
		* @param array  $options  (Optional) Associative array with
		*                         session configuration directives to
		*                         be overridden before starting a
		*                         session. The 'session.' prefix should
		*                         be removed from directive.
		*
		* @return true|false|null true if a session was started
		*                         successfully, false otherwise;
		*                         it returns null on some error
		*                         conditions
		*/
		function session_start_compat() {
			$result = null;

			$php_ver = phpversion();
			//$php_ver = '5.3.0';
			//$php_ver = '7.4.16';
			//$php_ver = '7.2.34';

			$args = func_get_args();
			$n_args = count($args);

			if (version_compare($php_ver, '8.0') >= 0) {
				$result = call_user_func_array('session_start', $args);
			} else {
				$error_strings = array(
					'headers_sent' => 'session_start(): Session cannot be started after headers have already been sent',
					'session_started' => 'session_start(): Ignoring session_start() because a session is already active',
					'setting' => 'session_start(): Option "%s" must be of type string|int|bool, %s given',
					'setting_x_failed' => 'session_start(): Setting option "%s" failed',
					'n_args' => 'session_start() expects at most 1 argument, %d given',
					'arg' => 'session_start(): Argument #1 ($options) must be of type array, %s given',
					'unknown_arg' => 'session_start() expects argument 1 to be array'
				);

				if ($n_args < 2) {
					if ($n_args === 0) {
						$args[0] = array();
					}

					if (is_array($args[0])) {
						if (version_compare($php_ver, '5.4') >= 0) {
							if (session_status() === PHP_SESSION_ACTIVE) {
								$invalid = trigger_error($error_strings['session_started'], E_USER_NOTICE);
								$result = true;
							}
						} else {
							$old_value = @ini_get('session.use_trans_sid');

							 if ($old_value !== @ini_set('session.use_trans_sid', $old_value)) {
								$invalid = trigger_error($error_strings['session_started'], E_USER_NOTICE);
								$result = true;
							 }
						}

						if (!isset($invalid)) {
							$samesite_available = version_compare($php_ver, '7.3') >= 0;
							$samesite_value = null;

							foreach ($args[0] as $setting => $value) {
								if (!(is_int($setting) || is_float($setting))) {
									if (is_array($value)) {
										throw new \TypeError(sprintf($error_strings['setting'], $setting, 'array'));
									} elseif (is_float($value)) {
										throw new \TypeError(sprintf($error_strings['setting'], $setting, 'float'));
									} elseif (is_object($value)) {
										throw new \TypeError(sprintf($error_strings['setting'], $setting, get_class($value)));
									} elseif ($value === null) {
										throw new \TypeError(sprintf($error_strings['setting'], $setting, 'null'));
									}

									if (!isset($invalid)) {
										// 'cookie_samesite' is available as of PHP 7.3
										if ($setting === 'cookie_samesite') {
											$samesite_value = $value;
										} else {
											if (@ini_set(sprintf('session.%s', $setting), $value) === false) {
												trigger_error(sprintf($error_strings['setting_x_failed'], $setting), E_USER_WARNING);
											}
										}
									}
								}
							}


							if (!isset($invalid)) {
								if (isset($args[0]['use_cookies'])) {
									$use_cookies = $args[0]['use_cookies'];
								} else {
									$use_cookies = @ini_get('session.use_cookies');
								}

								if (strtolower($use_cookies) !== 'off') {
									$use_cookies = (bool) $use_cookies;
								} else {
									$use_cookies = false;
								}

								if (!$use_cookies || !headers_sent()) {
									if (isset($args[0]['cookie_samesite'])) {
										if (!$samesite_available) {
											unset($args[0]['cookie_samesite']);
										} else {
											if (@ini_set(sprintf('session.%s', 'cookie_samesite'), $args[0]['cookie_samesite']) === false) {
												trigger_error(sprintf($error_strings['setting_x_failed'], 'cookie_samesite'), E_USER_WARNING);
											}
										}
									}

									$result = session_start();
								} else {
									$invalid = trigger_error($error_strings['headers_sent'], E_USER_WARNING);
									$result = @session_start();
								}

								if (empty($samesite_available)) {
									if ($samesite_value === null && isset($GLOBALS['session_start_compat_samesite_cache']))
										$samesite_value = $GLOBALS['session_start_compat_samesite_cache'];

									if (!isset($invalid) && $result && (!empty($samesite_value) || $samesite_value === 0)) {
										// 'SameSite' will be used only if sessions are using cookies.
										if ($use_cookies && $samesite_value !== null) {
											$session_name = @ini_get('session.name');

											if ($session_name !== false) {
												$headers = headers_list();
												header_remove();

												$needle = sprintf('Set-Cookie: %s', $session_name);

												// Append 'SameSite' attribute to already printed cookie header
												foreach ($headers as $header) {
													if (strpos($header, $needle) !== false) {
														$header .= sprintf('; SameSite=%s', $samesite_value);
														$GLOBALS['session_start_compat_samesite_cache'] = $samesite_value;
													}

													header($header, false);
												}
											}
										}
									}
								}
							}
						}
					} elseif (is_int($args[0])) {
						throw new \TypeError(sprintf($error_strings['arg'], 'int'));
					} elseif (is_float($args[0])) {
						throw new \TypeError(sprintf($error_strings['arg'], 'float'));
					} elseif (is_string($args[0])) {
						throw new \TypeError(sprintf($error_strings['arg'], 'string'));
					} elseif (is_bool($args[0])) {
						throw new \TypeError(sprintf($error_strings['arg'], 'bool'));
					} elseif (is_object($args[0])) {
						throw new \TypeError(sprintf($error_strings['arg'], get_class($args[0])));
					} elseif ($args[0] === null) {
						throw new \TypeError(sprintf($error_strings['arg'], 'null'));
					} else {
						throw new \TypeError($error_strings['unknown_arg']);
					}
				} else {
					throw new \ArgumentCountError(sprintf($error_strings['n_args'], $n_args));
				}
			}

			return $result;
		}
	}

	if (!function_exists('session_regenerate_id_compat')) {
		/**
		* Drop-in replacement for standard session_regenerate_id() allowing
		* older versions of PHP to start sessions using the PHP 8.0.3's
		* session_regenerate_id API
		*
		* @param bool  $delete_old_session  (Optional) `true` if it
		* should delete old associated session file; `false` otherwise.
		*
		* @return true|false `true` on success; `false` otherwise.
		*/
		function session_regenerate_id_compat($delete_old_session = false) {
			$result = session_regenerate_id($delete_old_session);

			$php_ver = phpversion();
			//$php_ver = '5.3.0';
			//$php_ver = '7.4.16';
			//$php_ver = '7.2.34';

			if ($result && version_compare($php_ver, '8.0') < 0) {
				$use_cookies = @ini_get('session.use_cookies');
				$samesite_value = @ini_get('session.cookie_samesite');
				$samesite_available = version_compare($php_ver, '7.3') >= 0;

				if (strtolower($use_cookies) !== 'off') {
					$use_cookies = (bool) $use_cookies;
				} else {
					$use_cookies = false;
				}

				if (!$samesite_available) {
					if (isset($GLOBALS['session_start_compat_samesite_cache'])) {
						$samesite_value = $GLOBALS['session_start_compat_samesite_cache'];
					}


					// 'SameSite' will be used only if sessions are using cookies.
					if ($use_cookies && !empty($samesite_value)) {
						$session_name = @ini_get('session.name');

						if ($session_name !== false) {
							$headers = headers_list();
							header_remove();

							$needle = sprintf('Set-Cookie: %s', $session_name);

							// Append 'SameSite' attribute to already printed cookie header
							foreach ($headers as $header) {
								if (strpos($header, $needle) !== false) {
									$header .= sprintf('; SameSite=%s', $samesite_value);
								}

								header($header, false);
							}
						}
					}
				}
			}

			return $result;
		}
	}
}

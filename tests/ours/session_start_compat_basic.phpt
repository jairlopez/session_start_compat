--TEST--
Test session_start_compat() for session : basic functionality
--SKIPIF--
<?php include('skipif.inc'); ?>
--INI--
date.timezone=UTC
session.save_path=
session.name="PHPSESSID"
session.save_handler="files"
session.auto_start="0"
session.gc_probability="1"
session.gc_divisor="100"
session.gc_maxlifetime="1440"
session.serialize_handler="php"
session.cookie_path="/"
session.cookie_domain=""
session.cookie_secure="0"
session.cookie_httponly="0"
session.cookie_samesite="Lax"
session.use_cookies="1"
session.use_only_cookies="1"
session.use_strict_mode="0"
session.referer_check=""
session.cache_limiter="nocache"
session.cache_expire="180"
session.use_trans_sid="0"
session.sid_length="32"
session.sid_bits_per_character="4"
session.lazy_write="1"
--FILE--
<?php
require __DIR__ . "/../../lib/session_start_compat.php";

ob_start();

echo "*** Testing session_start_compat() with an array as an argument ***\n\n";

session_start_compat(array(
	"name" => "PHP123SESSID",
	"gc_probability" => "2",
	"gc_divisor" => "99",
	"gc_maxlifetime" => "1500",
	"cookie_path" => "/tmp/",
	"cookie_domain" => "domain",
	"cookie_secure" => "1",
	"cookie_httponly" => "1",
	"cookie_samesite" => "Lax",
	"use_strict_mode" => "1",
	"referer_check" => "domain",
	"cache_expire" => "181"
));

var_dump("session started");

var_dump(ini_get("session.name"));
var_dump(ini_get("session.auto_start"));
var_dump(ini_get("session.gc_probability"));
var_dump(ini_get("session.gc_divisor"));
var_dump(ini_get("session.gc_maxlifetime"));
var_dump(ini_get("session.cookie_path"));
var_dump(ini_get("session.cookie_domain"));
var_dump(ini_get("session.cookie_secure"));
var_dump(ini_get("session.cookie_httponly"));
var_dump(ini_get("session.cookie_samesite"));
var_dump(ini_get("session.use_strict_mode"));
var_dump(ini_get("session.referer_check"));
var_dump(ini_get("session.cache_expire"));

$headers = headers_list();
var_dump($headers);
echo "Done";
ob_end_flush();

?>
--EXPECTHEADERS--

--EXPECTF--
*** Testing session_start_compat() with an array as an argument ***

string(15) "session started"
string(12) "PHP123SESSID"
string(1) "0"
string(1) "2"
string(2) "99"
string(4) "1500"
string(5) "/tmp/"
string(6) "domain"
string(1) "1"
string(1) "1"
string(3) "Lax"
string(1) "1"
string(6) "domain"
string(3) "181"
array(5) {
  [0]=>
  string(%d) "X-Powered-By: PHP/%s"
  [1]=>
  string(%d) "Set-Cookie: PHP123SESSID=%s; path=/tmp/; domain=domain; secure; HttpOnly; SameSite=Lax"
  [2]=>
  string(%d) "Expires: Thu, 19 Nov 1981 08:52:00 GMT"
  [3]=>
  string(%d) "Cache-Control: no-store, no-cache, must-revalidate"
  [4]=>
  string(%d) "Pragma: no-cache"
}
Done

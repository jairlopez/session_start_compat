--TEST--
Test session_start_compat() reports
--SKIPIF--
<?php include('skipif.inc'); ?>
--INI--
date.timezone=UTC
--FILE--
<?php
require __DIR__ . "/../../lib/session_start_compat.php";
ob_start();

echo "*** Testing session_start_compat() reports ***\n";

session_start_compat();

ob_end_flush();

session_start_compat();
session_destroy();
session_start_compat();

var_dump(headers_list());
?>
--EXPECTHEADERS--

--EXPECTF--
*** Testing session_start_compat() reports ***

Notice: session_start(): Ignoring session_start() because a session is already active in %s on line %d

Warning: session_start(): Session cannot be started after headers have already been sent in %s on line %d
array(6) {
  [0]=>
  string(%d) "X-Powered-By: PHP/%d.%d.%d"
  [1]=>
  string(%d) "Set-Cookie: PHPSESSID=%s; path=/"
  [2]=>
  string(38) "Expires: Thu, 19 Nov 1981 08:52:00 GMT"
  [3]=>
  string(50) "Cache-Control: no-store, no-cache, must-revalidate"
  [4]=>
  string(16) "Pragma: no-cache"
  [5]=>
  string(38) "Content-type: text/html; charset=UTF-8"
}

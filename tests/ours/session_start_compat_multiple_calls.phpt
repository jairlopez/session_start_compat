--TEST--
Test multiple calls to session_start_compat() function
--SKIPIF--
<?php include('skipif.inc'); ?>
--INI--
date.timezone=UTC
--FILE--
<?php
require __DIR__ . "/../../lib/session_start_compat.php";

ob_start();

echo "*** Testing session_destroy() : variation ***\n";

var_dump(session_start_compat(array("cookie_samesite" => "None")));
var_dump(headers_list());
var_dump(session_destroy());
var_dump(session_start_compat(array("cookie_samesite" => "Strict")));
var_dump(headers_list());
var_dump(session_destroy());
var_dump(session_start_compat(array("cookie_samesite" => "Lax")));
var_dump(headers_list());
var_dump(session_destroy());
var_dump(session_start_compat());
var_dump(headers_list());

echo "Done";
ob_end_flush();
?>
--EXPECTHEADERS--

--EXPECTF--
*** Testing session_destroy() : variation ***
bool(true)
array(5) {
  [0]=>
  string(%d) "X-Powered-By: PHP/%s"
  [1]=>
  string(%d) "Set-Cookie: PHPSESSID=%s; path=/; SameSite=None"
  [2]=>
  string(%d) "Expires: Thu, 19 Nov 1981 08:52:00 GMT"
  [3]=>
  string(%d) "Cache-Control: no-store, no-cache, must-revalidate"
  [4]=>
  string(%d) "Pragma: no-cache"
}
bool(true)
bool(true)
array(5) {
  [0]=>
  string(%d) "X-Powered-By: PHP/%s"
  [1]=>
  string(%d) "Set-Cookie: PHPSESSID=%s; path=/; SameSite=Strict"
  [2]=>
  string(%d) "Expires: Thu, 19 Nov 1981 08:52:00 GMT"
  [3]=>
  string(%d) "Cache-Control: no-store, no-cache, must-revalidate"
  [4]=>
  string(%d) "Pragma: no-cache"
}
bool(true)
bool(true)
array(5) {
  [0]=>
  string(%d) "X-Powered-By: PHP/%s"
  [1]=>
  string(%d) "Set-Cookie: PHPSESSID=%s; path=/; SameSite=Lax"
  [2]=>
  string(%d) "Expires: Thu, 19 Nov 1981 08:52:00 GMT"
  [3]=>
  string(%d) "Cache-Control: no-store, no-cache, must-revalidate"
  [4]=>
  string(%d) "Pragma: no-cache"
}
bool(true)
bool(true)
array(5) {
  [0]=>
  string(%d) "X-Powered-By: PHP/%s"
  [1]=>
  string(%d) "Set-Cookie: PHPSESSID=%s; path=/; SameSite=Lax"
  [2]=>
  string(%d) "Expires: Thu, 19 Nov 1981 08:52:00 GMT"
  [3]=>
  string(%d) "Cache-Control: no-store, no-cache, must-revalidate"
  [4]=>
  string(%d) "Pragma: no-cache"
}
Done

--TEST--
Test session_start_compat() reports
--SKIPIF--
<?php include('skipif.inc'); ?>
--INI--
date.timezone=UTC
--FILE--
<?php
require __DIR__ . "/../../lib/session_start_compat.php";
ob_start();

echo "*** Testing session_start_compat() reports ***\n\n";

// 01
try {
	session_start_compat(2020, 'one');
} catch (\ArgumentCountError $e) {
    echo $e->getMessage() . "\n";
}

// 02
try {
	session_start_compat(2020);
} catch (\TypeError $e) {
    echo $e->getMessage() . "\n";
}

// 03
try {
	session_start_compat(array(
		'two' => 'off',
		'three' => array(),
		'cookie_samesite' => array()
	));
} catch (\TypeError $e) {
    echo $e->getMessage() . "\n";
}

// 05
try {
	session_start_compat(3.14);
} catch (\TypeError $e) {
    echo $e->getMessage() . "\n";
}

// 06
try {
	session_start_compat('one');
} catch (\TypeError $e) {
    echo $e->getMessage() . "\n";
}

// 07
try {
	session_start_compat(false);
} catch (\TypeError $e) {
    echo $e->getMessage() . "\n";
}

// 08
class Foo {}

$bar = new Foo;

try {
	session_start_compat($bar);
} catch (\TypeError $e) {
    echo $e->getMessage() . "\n";
}

// 09
try {
	session_start_compat(null);
} catch (\TypeError $e) {
    echo $e->getMessage() . "\n";
}

// 10
try {
	session_start_compat(array(
		'cookie_samesite' => array()
	));
} catch (\TypeError $e) {
    echo $e->getMessage() . "\n";
}

// 11
try {
	session_start_compat(array(
		'cookie_samesite' => null
	));
} catch (\TypeError $e) {
    echo $e->getMessage() . "\n";
}


// 12
try {
	session_start_compat(array(
		'cookie_samesite' => 3.14
	));
} catch (\TypeError $e) {
    echo $e->getMessage() . "\n";
}

// 13
try {
	session_start_compat(array(
		'cookie_samesite' => $bar
	));
} catch (\TypeError $e) {
    echo $e->getMessage() . "\n";
}

// 14
try {
	session_start_compat(array(
		'cookie_domain' => array()
	));
} catch (\TypeError $e) {
    echo $e->getMessage() . "\n";
}

// 15
try {
	session_start_compat(array(
		'cookie_domain' => null
	));
} catch (\TypeError $e) {
    echo $e->getMessage() . "\n";
}

// 16
try {
	session_start_compat(array(
		'cookie_domain' => 3.14
	));
} catch (\TypeError $e) {
    echo $e->getMessage() . "\n";
}

// 16
try {
	session_start_compat(array(
		'cookie_domain' => $bar
	));
} catch (\TypeError $e) {
    echo $e->getMessage() . "\n";
}
ob_end_flush();
?>
--EXPECTHEADERS--

--EXPECTF--
*** Testing session_start_compat() reports ***

session_start() expects at most 1 argument, 2 given
session_start(): Argument #1 ($options) must be of type array, int given

Warning: session_start(): Setting option "two" failed in %s on line %d
session_start(): Option "three" must be of type string|int|bool, array given
session_start(): Argument #1 ($options) must be of type array, float given
session_start(): Argument #1 ($options) must be of type array, string given
session_start(): Argument #1 ($options) must be of type array, bool given
session_start(): Argument #1 ($options) must be of type array, Foo given
session_start(): Argument #1 ($options) must be of type array, null given
session_start(): Option "cookie_samesite" must be of type string|int|bool, array given
session_start(): Option "cookie_samesite" must be of type string|int|bool, null given
session_start(): Option "cookie_samesite" must be of type string|int|bool, float given
session_start(): Option "cookie_samesite" must be of type string|int|bool, Foo given
session_start(): Option "cookie_domain" must be of type string|int|bool, array given
session_start(): Option "cookie_domain" must be of type string|int|bool, null given
session_start(): Option "cookie_domain" must be of type string|int|bool, float given
session_start(): Option "cookie_domain" must be of type string|int|bool, Foo given

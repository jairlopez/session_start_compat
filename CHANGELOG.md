# Change Log

## 2.0.0

- *Date:* April 9th, 2020

* Throw exceptions like PHP 8.0.3's `session_start`

## 1.0.0

- *Date:* March 30th, 2020

Initial release
